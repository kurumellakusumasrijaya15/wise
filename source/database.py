import sqlite3

# Function to create tables if they don't exist
def create_tables(conn):
    cursor = conn.cursor()
    cursor.execute("""CREATE TABLE IF NOT EXISTS users (
                        id INTEGER PRIMARY KEY,
                        username TEXT,
                        password TEXT
                    )""")
    conn.commit()

# Function to insert a user into the database
def insert_user(conn, username, password):
    cursor = conn.cursor()
    cursor.execute("INSERT INTO users (username, password) VALUES (?, ?)", (username, password))
    conn.commit()

# Function to check if a user exists in the database
def check_user(conn, username, password):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM users WHERE username=? AND password=?", (username, password))
    user = cursor.fetchone()
    if user:
        return True
    else:
        return False

# Connect to the database
conn = sqlite3.connect('example.db')

# Create tables if they don't exist
create_tables(conn)

# Insert a user
insert_user(conn, 'john_doe', 'password123')

# Check if a user exists
if check_user(conn, 'john_doe', 'password123'):
    print("User found!")
else:
    print("User not found!")

# Close the connection
conn.close()
