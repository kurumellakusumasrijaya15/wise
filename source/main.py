from tkinter import Tk, Frame, Label, Entry, Button, messagebox
from PIL import Image, ImageTk
import sqlite3

class Home:
    def __init__(self, root):
        self.root = root
        root.resizable(0,0)
        self.frame = Frame(self.root, width=550, height= 350, bd= 4, relief= 'ridge', bg= 'white')
        self.frame.place(x = 0,y = 0)

        self.bg   = Image.open('C:/Users/HP/Desktop/Wise/assets/bg1.png')
        self.bg = self.bg.resize((550, 350))
        self.bg = ImageTk.PhotoImage(self.bg)
        self.bg_lbl = Label(self.frame, image= self.bg)
        self.bg_lbl.place(x= 0, y = 0)

        self.lbl=  Label(self.frame, text= 'USER NAME', bg = 'white', fg = 'steel blue', font=('Courier New', 20, 'bold'))
        self.lbl.place(x = 50, y = 150)

        self.username_entry = Entry(self.frame, width = 15, font=('Courier New', 18, 'bold'), bg = 'white')
        self.username_entry.place(x = 220, y = 150)

        self.lbl=  Label(self.frame, text= 'PASSWORD', bg = 'white', fg = 'steel blue', font=('Courier New', 20, 'bold'))
        self.lbl.place(x = 50, y = 200)

        self.password_entry = Entry(self.frame, width = 15, font=('Courier New', 18, 'bold'), bg = 'white')
        self.password_entry.place(x = 220, y = 200)
        
        self.login_btn = Button(self.frame, text = 'LOGIN',font=('Courier New', 18, 'bold'), bg = 'green', command= self.check_login)
        self.login_btn.place(x = 150 , y = 250)

        self.signin_btn = Button(self.frame, text = 'Sign In',font=('Courier New', 18, 'bold'), bg = 'green', command= self.sign_in)
        self.signin_btn.place(x = 275, y = 250)

        self.db = Database()

    def check_login(self):
        username = self.username_entry.get()
        password = self.password_entry.get()
        if self.db.check_user(username, password):
            self.frame.destroy()
            self.details()
        else:
            messagebox.showerror("Login Error", "Invalid username or password")

    def sign_in(self):
        username = self.username_entry.get()
        password = self.password_entry.get()
        if not self.db.check_user_exists(username):
            self.db.insert_user(username, password)
            messagebox.showinfo("Success", "User signed in successfully")
        else:
            messagebox.showerror("Sign In Error", "Username already exists")

    def details(self):
        self.frame = Frame(self.root, width=550, height= 350, bd= 4, relief= 'ridge', bg= 'white')
        self.frame.place(x = 0,y = 0)
        self.lbl=  Label(self.frame, text= 'Name', bg = 'white', fg = 'steel blue', font=('Courier New', 20, 'bold'))
        self.lbl.place(x = 80, y = 50)
        self.lbl=  Label(self.frame, text= 'Age', bg = 'white', fg = 'steel blue', font=('Courier New', 20, 'bold'))
        self.lbl.place(x = 80, y = 80)
        self.lbl=  Label(self.frame, text= 'Gender', bg = 'white', fg = 'steel blue', font=('Courier New', 20, 'bold'))
        self.lbl.place(x = 80, y = 110)
        self.lbl=  Label(self.frame, text= 'From', bg = 'white', fg = 'steel blue', font=('Courier New', 20, 'bold'))
        self.lbl.place(x = 80, y = 140)
        self.lbl=  Label(self.frame, text= 'To', bg = 'white', fg = 'steel blue', font=('Courier New', 20, 'bold'))
        self.lbl.place(x = 80, y = 170)

        self.name_entry = Entry(self.frame, width = 15, font=('Courier New', 12, 'bold'), bg = 'white')
        self.name_entry.place(x = 220, y = 50)
        self.age_entry = Entry(self.frame, width = 15, font=('Courier New', 12, 'bold'), bg = 'white')
        self.age_entry.place(x = 220, y = 80)
        self.gender_entry = Entry(self.frame, width = 15, font=('Courier New', 12, 'bold'), bg = 'white')
        self.gender_entry.place(x = 220, y = 110)
        self.from_entry = Entry(self.frame, width = 15, font=('Courier New', 12, 'bold'), bg = 'white')
        self.from_entry.place(x = 220, y = 140)
        self.to_entry = Entry(self.frame, width = 15, font=('Courier New', 12, 'bold'), bg = 'white')
        self.to_entry.place(x = 220, y = 170)
        
        self.submit_btn = Button(self.frame, text = 'Submit',font=('Courier New', 18, 'bold'), bg = 'green', command= self.submit_details)
        self.submit_btn.place(x = 190, y = 210)

    def submit_details(self):
        name = self.name_entry.get()
        age = self.age_entry.get()
        gender = self.gender_entry.get()
        from_location = self.from_entry.get()
        to_location = self.to_entry.get()

        # Here you can add code to store these details in the database if needed
        self.db.insert_details(name, age, gender)

        messagebox.showinfo("Success", "Details submitted successfully")
        self.frame.destroy()
        self.next_page()

    def next_page(self):
        next_frame = Frame(self.root, width=550, height= 350, bd= 4, relief= 'ridge', bg= 'white')
        next_frame.place(x = 0,y = 0)
        lbl = Label(next_frame,text="successfully request sent,Waiting for approval", bg='white', fg='steel blue', font=('Courier New',12, 'bold'))
        lbl.place(x=50, y=150)
class Database:
    def __init__(self):
        self.conn = sqlite3.connect('example.db')
        self.cur = self.conn.cursor()
        self.create_tables()

    def create_tables(self):
        self.cur.execute("""CREATE TABLE IF NOT EXISTS users (
                            id INTEGER PRIMARY KEY,
                            username TEXT,
                            password TEXT
                            )""")
        self.cur.execute("""CREATE TABLE IF NOT EXISTS details (
                            id INTEGER PRIMARY KEY,
                            name TEXT,
                            age INTEGER,
                            gender TEXT
                            )""")
        self.conn.commit()

    def insert_user(self, username, password):
        self.cur.execute("INSERT INTO users (username, password) VALUES (?, ?)", (username, password))
        self.conn.commit()

    def check_user(self, username, password):
        self.cur.execute("SELECT * FROM users WHERE username=? AND password=?", (username, password))
        user = self.cur.fetchone()
        if user:
            return True
        else:
            return False

    def check_user_exists(self, username):
        self.cur.execute("SELECT * FROM users WHERE username=?", (username,))
        user = self.cur.fetchone()
        if user:
            return True
        else:
            return False

    def insert_details(self, name, age, gender):
        self.cur.execute("INSERT INTO details (name, age, gender) VALUES (?, ?, ?)", (name, age, gender))
        self.conn.commit()

    def close(self):
        self.conn.close()

root = Tk()
root.title('Online Bus Pass Application')
root.geometry('550x350+550+200')
home = Home(root)
root.mainloop()

