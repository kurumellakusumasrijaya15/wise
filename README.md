# Online Bus Pass Application

## Description
This is an Online Bus Pass Application developed using Python and Tkinter. It allows users to sign in, submit their details, and request a bus pass online. The application provides a convenient alternative to the traditional method of applying for bus passes by eliminating the need to visit the bus depot and stand in long queues.

## Features
- User authentication (login and sign up)
- Form for submitting user details (name, age, gender, from, to)
- Storage of user details in a SQLite database
- Confirmation message upon successful submission of details
- Waiting for approval message after submitting the request for a bus pass

## Requirements
- Python
- Tkinter
- Pillow (PIL)
- SQLite3

## Installation
1. Clone the repository: git clone https://github.com/kurumellakusumasrijaya15/Wise.git
2. To clone repository : git clone https://gitlab.com/username/project-name.git
3. Install Python from the [official website](https://www.python.org/downloads/).
4. Install pillow: pip install pillow

## Usage
1. Run the application: `python main.py`
2. Sign in or create a new account if you don't have one.
3. Fill in the required details in the submission form.
4. Click on the "Submit" button to submit the details.
5. Upon successful submission, a confirmation message will be displayed.
6. Wait for the approval message to know the status of your bus pass request.

##Contributors
-[Kurumella kusuma sri jaya](https://gitlab.com/kurumellakusumasrijaya15)
-[Matta Devi](https://gitlab.com/devireddymatta)
-[Balagam sireesha](https://gitlab.com/balagamsaisireesha22)
-[Subba lakshmi](https://gitlab.com/kurumellakusumasrijaya15)
-[sri Ramya](https://gitlab.com/kurumellakusumasrijaya15)
## License
This project is licensed under the [MIT License](https://gitlab.com/kurumellakusumasrijaya15/wise/-/blob/master/LICENSE?ref_type=heads).
